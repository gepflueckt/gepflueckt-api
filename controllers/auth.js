// Load required packages
var Config = require('../config.js'),
		passport = require('passport'),
		User = require('../models/User'),
		JwtStrategy = require('passport-jwt').Strategy,
		jwt = require('jsonwebtoken'),
    ExtractJwt = require('passport-jwt').ExtractJwt;

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromHeader("authorization");
jwtOptions.secretOrKey = Config.secret;

passport.use(new JwtStrategy(jwtOptions, function(jwt_payload, next) {

	console.log('payload received:', jwt_payload);

  User.findOne( { _id: jwt_payload._id }, function(err, user) {
    if (err) return next(err, false);

		if (user) {
      return next(null, user);
    } else {
      return next(null, false);
    }
  });

}));

let loosePassport = (req, res, next) => {
	passport.authenticate('jwt', { session: false }, (err, user, info) => {
		if (err) throw err;
		req.user = user;
		next();
	})(req, res, next)
}

exports.authenticate = {
	strict: passport.authenticate('jwt', { session: false }),
	loose: loosePassport
}

exports.login = (req, res, next) => {
	let nick = req.body.nick,
			password = req.body.password;

	User.findOne( { nick: nick } , (err, user) => {
		if (err) throw err;

		if (!user) { return res.json({ success: false, message: 'User not found' }); }

		user.verifyPassword(password, (err, isMatch) => {
			if (err) throw err;

			if (!isMatch) { return res.json({ success: false, message: 'Wrong password' }); }

			else {
				// Success
				let token = jwt.sign(user.toObject(), Config.secret, { expiresIn: 604800 });

				return res.status(200).json({
					success: true,
					token: token,
					user: {
						id : user._id,
						nick : user.nick,
						email : user.email,
						role: user.role
					}
				})

			}
		})
	})
}
