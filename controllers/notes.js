var mongoose = require('mongoose');

let Note = require('../models/Note');

let NotesController = {

	all : (req, res) => {
		Note.find({}, (err, notes) => {
			if(err) throw err;

			res.json(notes);
		})
	},

	byId : (req, res) => {
		Note.findById(req.params.id, (err, notes) => {
			if(err) throw err;

			res.json(notes);
		})
	},

	new : (req, res) => {

		let theNote = new Note(req.body);

		// test
		console.log('creating user...', req.body);

		theNote.save((err, notes) => {
			if(err) throw err;

			res.json(notes);
		})
	},

	update : (req, res) => {

		Note.findById(req.params.id, (err, notes) => {
			if(notes) {
				notes.set(req.body);

				notes.save((err, notes) => {
					if(err) throw err;

					res.json(notes);
				})
			}
			else res.json([]);
		})

	},

	delete : (req, res) => {
		Note.findByIdAndRemove(req.params.id, (err, notes) => {
			if(err) throw err;

			res.json(notes);
		})
	}

}

module.exports = NotesController;
