var mongoose = require('mongoose');

let Product = require('../models/Product');
let User = require('../models/User');

let ProductsController = {

	all : (req, res) => {

		Product.find({})
		// Authorization
		// Populate only if logged in and not deactivated
		.populate( ( req.user && req.user.role > 1 ) ? 'author' : '' )
		.exec((err, products) => {
			if(err) throw err;

			User.findById(products.author, (err, author) => {
				if(err) throw err;

				if(author) products.author = author;
			})

			res.json(products);
		});

	},

	byId : (req, res) => {
		Product.findById(req.params.id)
		// Authorization
		// Populate only if logged in and not deactivated
		.populate( ( req.user && req.user.role > 1 ) ? 'author' : '' )
		.exec((err, products) => {
			if(err) throw err;

			res.json(products);
		})
	},

	byAuthor : (req, res) => {

		// Authorization (Only Admin can view products by author)
		if( req.user.role < 3 ) res.status(401).json("Unauthorized");

		Product.find({ author : req.params.user_id })
		.populate('author')
		.exec((err, products) => {
			if(err) throw err;

			res.json(products);
		})

	},

	new : (req, res) => {

		// Authorization
		if( req.user.role < 1 ) res.status(401).json("Unauthorized");

		let theProduct = new Product(req.body.data);

		theProduct.save((err, products) => {
			if(err) throw err;

			res.json(products);
		})
	},

	update : (req, res) => {

		// Authorization
		if( req.user.role < 1 ) res.status(401).json("Unauthorized");

		Product.findById(req.params.id, (err, products) => {
			if(products) {


				// Authorization
				if( req.user.role < 2 && req.user.id != products.author ) res.status(401).json("Unauthorized");

				products.set(req.body);

				products.save((err, products) => {
					if(err) throw err;

					res.json(products);
				})
			}
			else res.json([]);
		})

	},

	delete : (req, res) => {

		// Authorization
		if( req.user.role < 1 ) res.status(401).json("Unauthorized");

		Product.findById(req.params.id, (err, products) => {
			if(products) {
				// Authorization
				if( req.user.role < 2 && req.user.id != products.author ) res.status(401).json("Unauthorized");

				products.remove();
			}
			else res.json([]);
		})

	}

}

module.exports = ProductsController;
