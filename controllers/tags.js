var mongoose = require('mongoose');

let Tag = require('../models/Tag');

let TagsController = {

	all : (req, res) => {
		Tag.find({ parent: null })
		.populate('children')
		.exec((err, tags) => {
			if(err) throw err;

			res.json(tags);
		})
	},

	byId : (req, res) => {
		Tag.findById(req.params.id, (err, tags) => {
			if(err) throw err;

			res.json(tags);
		})
	},

	new : (req, res) => {

		let theTag = new Tag(req.body.data);
		let parent = req.body.data.parent;

		theTag.save((err, tags) => {
			if(err) throw err;

			// If parent is set, update that parents children
			if(parent) {
				Tag.findOneAndUpdate(
					{ _id : parent },
					{ $addToSet : { children : tags._id } },
					(err) => { if(err) throw err; }
				)
			}

			res.json(tags);
		})
	},

	update : (req, res) => {

		Tag.findById(req.params.id, (err, tags) => {
			if(tags) {
				tags.set(req.body);

				tags.save((err, tags) => {
					if(err) throw err;

					res.json(tags);
				})
			}
			else res.json([]);
		})

	},

	delete : (req, res) => {
		Tag.findByIdAndRemove(req.params.id, (err, tags) => {
			if(err) throw err;

			res.json(tags);
		})
	}

}

module.exports = TagsController;
