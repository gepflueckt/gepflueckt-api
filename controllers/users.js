var mongoose = require('mongoose');

let User = require('../models/User');

let UsersController = {

	all : (req, res) => {

		// Authorization
		if( req.user.role < 3 ) res.status(401).json("Unauthorized");

		User.find({}, (err, users) => {

			if(err) throw err;

			res.json(users);
		})
	},

	byId : (req, res) => {

		// Authorization
		if( req.user.role < 3 && req.user.id != req.params.id ) res.status(401).json("Unauthorized");

		User.findById(req.params.id, (err, users) => {
			if(err) throw err;

			res.json(users);
		})
	},

	new : (req, res) => {

		// Authorization
		if( req.user.role < 3 ) res.status(401).json("Unauthorized");

		let theUser = new User(req.body);

		// test
		console.log('creating user...', req.body);

		theUser.save((err, users) => {
			if(err) throw err;

			res.json(users);
		})
	},

	update : (req, res) => {

		// Authorization
		if( req.user.role < 3 && ( req.user.id != req.params.id || role in req.body ) ) res.status(401).json("Unauthorized");

		User.findById(req.params.id, (err, users) => {
			if(users) {

				users.set(req.body);

				users.save((err, users) => {
					if(err) throw err;

					res.json(users);
				})
			}
			else res.json([]);
		})

	},

	delete : (req, res) => {

		// Authorization
		if( req.user.role < 3 ) res.status(401).json("Unauthorized");

		User.findByIdAndRemove(req.params.id, (err, users) => {
			if(err) throw err;

			res.json(users);
		})
	}

}

module.exports = UsersController;
