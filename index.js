// Load Config
let Config = require('./config.js');

// Config
let port = Config.port,
		apiroute = Config.baseRoute;

// MongoDB Connection
var mongoose = require('mongoose');
mongoose.connect(Config.mongodb)

// Express stuff
var express = require('express')
var session = require('express-session')
var bodyParser = require('body-parser')

// Passport
var passport = require('passport')

var app = express()
var router = express.Router()

// Configure App
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(passport.initialize());
app.use(passport.session( { secret : Config.secret } ));
app.use('/api', router)

app.listen(port, () => { console.log('Gepflückt API running on ' + port) })

// Controllers
var ProductsController = require('./controllers/products.js')
var Auth = require('./controllers/auth.js')
var UsersController = require('./controllers/users.js')
var TagsController = require('./controllers/tags.js')
var NotesController = require('./controllers/notes.js')



// Standard response
router.get('/', (req, res) => {
  res.json({ message: 'Gepflückt API' })
})

// ROUTES

router.route('/login')
	.get(Auth.login)

router.route('/logout')
.get((req, res) => {
	req.logout();
	res.status(200).json("Logout successful");
})

/*
* PRODUCTS
*/
router.route('/products')
	.post(Auth.authenticate.strict, ProductsController.new)
	.get(Auth.authenticate.loose, ProductsController.all)

router.route('/products/:id')
	.get(ProductsController.byId)
	.put(Auth.authenticate.strict, ProductsController.update)
	.delete(Auth.authenticate.strict, ProductsController.delete)

router.route('/products/by/:user_id')
	.get(Auth.authenticate.strict, ProductsController.byAuthor)

/*
* USERS
*/
router.route('/users')
	.post(Auth.authenticate.strict, UsersController.new)
	.get(Auth.authenticate.strict, UsersController.all)

router.route('/users/:id')
	.get(Auth.authenticate.strict, UsersController.byId)
	.put(Auth.authenticate.strict, UsersController.update)
	.delete(Auth.authenticate.strict, UsersController.delete)

/*
* TAGS
*/
router.route('/tags')
	.post(Auth.authenticate.strict, TagsController.new)
	.get(TagsController.all)

router.route('/tags/:id')
	.get(TagsController.byId)
	.put(Auth.authenticate.strict, TagsController.update)
	.delete(Auth.authenticate.strict, TagsController.delete)

/*
* NOTES
*/
router.route('/notes')
	.post(Auth.authenticate.strict, NotesController.new)
	.get(Auth.authenticate.strict, NotesController.all)

router.route('/notes/:id')
	.get(Auth.authenticate.strict, NotesController.byId)
	.put(Auth.authenticate.strict, NotesController.update)
	.delete(Auth.authenticate.strict, NotesController.delete)
