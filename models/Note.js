var mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Note = new Schema({

	author: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true
	},

	title: {
		type: String,
		required: true
	},

	content: {
		type: String,
		required: true
	},

	type: {
		type: Number,
		min: 1,
		max: 10,
		default: 1
	}

})

module.exports = mongoose.model('Note', Note);
