var mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Product = new Schema({

	title: {
		type: String,
		required: true
	},

	description: {
		type: String,
		required: true
	},

	asin : {
		type: String,
		required: true
	},

	product_url: {
		type: String,
		required: true
	},

	image_url: {
		type: String,
		required: true
	},

	clicks: {
		type: Number,
		default: 0
	},

	tags: [{
		type: Schema.ObjectId,
		ref: 'Tag'
	}],

	author: {
		type: Schema.ObjectId,
		ref: 'User'
	},

	status: {
		type: Number,
		min: 0,
		max: 10,
		default: 0
	},

	notes: [{
		type: Schema.ObjectId,
		ref: 'Note'
	}]

})

module.exports = mongoose.model('Product', Product);
