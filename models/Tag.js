var mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Tag = new Schema({

	name: {
		type: String,
		required: true
	},

	parent: {
		type: Schema.ObjectId,
		ref: 'Tag'
	},

	children: [{
		type: Schema.ObjectId,
		ref: 'Tag'
	}],

	significance: {
		type: Number,
		min: 1,
		max: 100,
		default: 50
	}

})

module.exports = mongoose.model('Tag', Tag);
