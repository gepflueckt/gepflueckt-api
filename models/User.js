var mongoose = require('mongoose'),
		bcrypt = require('bcrypt');

let Schema = mongoose.Schema;

let User = new Schema({

	nick: {
		type: String,
		required: true
	},

	email: {
		type: String,
		required: true
	},

  password: {
    type: String,
    required: true
  },

	role: {
		type: Number,
		required: true,
		min: 0,
		max: 10
	}

	// 0 - disabled
	// 1 - creators
	// 2 - editors
	// 3 - admin

})

// Password stuff before saving
User.pre('save', function(callback) {
  var user = this

	console.log('pre save');

	// Check if new or modified password
  if (!user.isModified('password'))
      return callback();

  // Password changed, gen salt...
  bcrypt.genSalt(5, function(err, salt) {
    if (err) throw err;

		console.log('Salt generated...');

		// ...and hash dat!
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) throw err;

			console.log('Hash generated too!');

      user.password = hash
      callback()
    });
  });
})

// Verifying passwords
User.methods.verifyPassword = function(password, cb) {

	bcrypt.compare(password, this.password, (err, isMatch) => {
		if (err) return cb(err)

		cb(null, isMatch)
	})
}

module.exports = mongoose.model('User', User);
